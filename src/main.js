import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@vant/touch-emulator';
import {
    Button,
    NavBar,
    Grid,
    GridItem,
    Pagination,
    Field,
    Toast ,
}
    from 'vant';

Vue.config.productionTip = false

Vue.use(Button)
    .use(NavBar)
    .use(Grid)
    .use(GridItem)
    .use(Pagination)
    .use(Field)
    .use(Toast)

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
