import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        component: () => import( '@/views/Index'),
        children: [
            {
                path: '',
                name: 'Home',
                meta: {name: '首页'},
                hidden: true,
                component: () => import( '@/views/home/Home'),
            },
            {
                path: 'GenshinImpact',
                name: 'GenshinImpact',
                meta: {name: '首页'},
                hidden: true,
                component: () => import( '@/views/genshinImpact/GenshinImpact'),
            },
        ]
    },

]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
